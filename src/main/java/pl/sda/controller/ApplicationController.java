package pl.sda.controller;

import pl.sda.model.Car;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Managed bean to klasa kontrolera, którą będzie można wywołać w widoku
 * w tym przypadku poprzez #{applicationController}
 * <p>
 * Stan tej klasy bedzie dostępny poprzez cały czas istnienia aplikacji
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {

    private EntityManagerFactory emf;
    private EntityManager em;
    private String data;

    public ApplicationController() {
        emf = Persistence.createEntityManagerFactory("JSFCrudPU");
        em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Car car = new Car();
        car.setMass(1400);
        car.setRegPlate("SK 110KL");
        car.setVin("qwertyuio");
        em.persist(car);
        transaction.commit();
        System.out.println("Baza danych zainicjalizowana");
    }

    public EntityManager getEntityManager() {
        return em;
    }
}
