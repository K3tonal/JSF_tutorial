package pl.sda;

import pl.sda.model.Car;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * Managed bean to klasa kontrolera, którą będzie można wywołać w widoku
 * w tym przypadku poprzez #{applicationController}
 * <p>
 * Stan tej klasy bedzie dostępny poprzez cały czas istnienia aplikacji
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public ApplicationController(){
        emf = Persistence.createEntityManagerFactory("JSFCrudPU");
        em = emf.createEntityManager();
    }

    public EntityManager getEntityManager(){
        return em;
    }

    public List<Car> getList(){
        Query query = em.createQuery("from pl.sda.model.Car c");
        return query.getResultList();
    }

}
